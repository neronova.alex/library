<?php

/**
 * @return \App\Models\User|null
 */
function auth_user(): ?App\Models\User
{
    if (session()->exists('user_id'))
    {
        return App\Models\User::find(session()->get('user_id'));
    } else {
        return NULL;
    }
}
