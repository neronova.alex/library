<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApplicationRequest;
use App\Models\Application;
use App\Models\Book;
use App\Models\Category;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ApplicationsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Book $book)
    {
        $library_card = auth_user()->library_card;
        return view('applications.create', compact('book', 'library_card'));
    }


    /**
     * @param ApplicationRequest $request
     * @param Book $book
     * @param User $user
     * @return RedirectResponse
     */
    public function store(ApplicationRequest $request, Book $book, User $user): RedirectResponse
    {
        if($book->status == 'get') {
            $book->status = 'expected';
        } else {
            $book->status = 'get';
        }
        $return_date = $request->input('return_date');
        $book->return_date = Carbon::parse($return_date)->format('Y-m-d');

        $book->save();

        $application = new Application();
        $application->end_date = $request->input('end_date');
        $application->user_id = session()->get('user_id');
        $application->book_id = $book->id;
        $application->save();

        return redirect()->route('categories.index', ['user->id' => $user->id, 'body_>id' => $book->id])->with('status', "Application successfully filled!");
    }
}
