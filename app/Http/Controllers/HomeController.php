<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function home()
    {
        if(session()->exists('user_id')) {
            return view('layouts.main');
        }
        return redirect()->route('sessions.login');
    }
}
