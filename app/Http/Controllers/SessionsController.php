<?php

namespace App\Http\Controllers;

use App\Http\Requests\SessionRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SessionsController extends AuthController
{
    public function create(Request $request)
    {
        if ($request->session()->exists('user_id')) {
            return redirect()->route('categories.index')->with('error', 'You are already login!');
        }
        return view('sessions.create');
    }

    /**
     * @param SessionRequest $request
     * @return RedirectResponse
     */
    public function store(SessionRequest $request): RedirectResponse
    {
        $user_collection = User::where('library_card', $request->get('library_card'))->get();
        if ($user_collection->isNotEmpty()) {
            $user = $user_collection->first();
            if ($this->auth($user, $request->get('password'))) {
                $this->logIn($user);
                return redirect()->route('home')->with('success', 'You are log in successfully!');
            }
        }

        return redirect()->back()->with('error', 'Incorrect library card or password!');
    }

    /**
     * @return RedirectResponse
     */
    public function destroy()
    {
        $this->logOut();
        return redirect()->route('sessions.login')->with('success', 'You are log out successfully!');
    }
}
