@extends('layouts.base')

@section('content')
    @include('notifications.alerts')
    <div class="container">
        <div class="col-6">
            <div class="row">
                <div class="col-2">
                    <h3>Genres</h3>
                </div>
            </div>
        </div>
    </div>

    @foreach($categories as $category)
        <a href="{{route('categories.show', ['category' => $category])}}"><h3>{{$category->genre}}</h3></a>
        <div class="container">
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-center">
                @foreach($books as $book)
                    @if($category->id == $book->category_id)
                        <div class="d-flex col mb-3">
                            <div class="card">
                                <div class="card-img">
                                    <img src="{{asset('/storage/' . $book->picture)}}" class="card-img-top" alt="{{$book->picture}}">
                                </div>
                                <div class="card-body d-flex flex-column">
                                    <a href="{{route('books.show', ['book' => $book])}}"><b>{{$book->title}}</b></a>
                                    <p class="card-text">{{$book->author}}</p>
                                        <form id="create-application" action="{{route('books.applications.create', ['book' => $book])}}">
                                                @if($book->status == 'get')
                                                <button type="submit" class="btn btn-outline-primary btn-sm btn-block" role="button"
                                                        aria-pressed="true">Get</button>
                                            @else
                                                <p>Expected date: {{$book->return_date}}</p>
                                            @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    @endforeach

    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $books->links('pagination::bootstrap-4') }}
        </div>
    </div>

@endsection
