@extends('layouts.base')

@section('content')

    <div class="container">
        <div>
            <h1>{{$category->genre}}</h1>
        </div>

        <div class="mt-3 mb-3">
            <a href="{{route('categories.index')}}" type="button" class="btn btn-outline-primary">
                <b>Back</b>
            </a>
        </div>

            <div class="container">
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-center">
                    @foreach($category->books as $book)

                            <div class="d-flex col mb-3">
                                <div class="card">
                                    <div class="card-img">
                                        <img src="{{asset('/storage/' . $book->picture)}}" class="card-img-top" alt="{{$book->picture}}">
                                    </div>
                                    <div class="card-body d-flex flex-column">
                                        <h5 class="card-title"><b>{{$book->title}}</b></h5>
                                        <p class="card-text">{{$book->author}}</p>
                                        <div class="mt-auto" id="{{$book->id}}">
                                            <a href="{{route('applications.create', ['application' => $application])}}" class="btn btn-primary"><b>Get</b></a>
                                            <button class="btn btn-primary btn-add-to-cart"><b>Get</b></button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                    @endforeach
                </div>
            </div>
    </div>

@endsection
