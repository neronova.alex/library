@extends('layouts.base')

@section('content')
    @include('notifications.alerts')

    <div class="col main pt-5 mt-3">
        <h1 class="display-4 d-none d-sm-block">
            {{$book->title}}
        </h1>
        <div class="card" style="width: 26rem">
            <img class="card-img-top" src="{{asset('/storage/' . $book->picture)}}" alt="{{$book->picture}}" width="25" height="500">
            <div class="card-body">
                <p class="card-text"><b>Author:</b> {{$book->author}}
            </div>
            <div class="card-body">
                <a href="{{route('categories.index')}}" class="btn btn-primary btn-sm"><b>Back</b></a>
            </div>
        </div>
    </div>


@endsection
