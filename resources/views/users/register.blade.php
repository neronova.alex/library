@extends('layouts.base')

@section('content')

    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('notifications.alerts')

            <h1 class="text-center">Register</h1>
            <hr>
            <form action="{{route('users.store')}}" method="post">
                @csrf

                <div class="form-group">
                    <label for="full_name">Full name</label>
                    <input type="text" class="form-control" id="full_name" name="full_name">
                </div>

                <div class="form-group">
                    <label for="exampleInputAddress">Address</label>
                    <input type="text" class="form-control" id="exampleInputAddress" aria-describedby="addressHelp" name="address">
                    <small id="addressHelp" class="form-text text-muted">We'll never share your address with anyone else.</small>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassport">Passport</label>
                    <input type="text" class="form-control" id="exampleInputPassport" aria-describedby="passportHelp" name="passport">
                    <small id="passportHelp" class="form-text text-muted">We'll never share your passport with anyone else.</small>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword2">Confirm password</label>
                    <input type="password" class="form-control" id="exampleInputPassword2" name="password_confirmation">
                </div>

                <div class="form-group">
                    <input type="hidden" class="form-control" id="exampleInputLibrary_card1" name="library_card" value="{{random_int(100000, 500000)}}">
                </div>
                <button type="submit" class="btn btn-outline-primary">Get library card</button>
            </form>
        </div>
    </div>

@endsection
