@extends('layouts.base')

@section('content')
    @include('notifications.alerts')

        <div class="col main pt-5 mt-3">
            <h1>Get library book</h1>
            <form enctype="multipart/form-data" method="post" action="{{ route('books.applications.store',
 ['book' => $book])}}">
                @csrf
                <div class="form-group">
                    <label for="title"><b>Library card</b></label>
                    <h3>{{$library_card}}</h3>
                </div>
                <div class="form-group">
                    <label for="book_id"><b>Book</b></label>
                    <option>{{$book->title}}, {{$book->author}}</option>
                </div>

                <div class="form-group">
                        <div>
                            <label for="end_date">Date</label>
                            <input type="date" id="end_date" name="end_date">
                        </div>
                </div>

                <button type="submit" class="btn btn-primary">Apply</button>
            </form>
        </div>

@endsection
