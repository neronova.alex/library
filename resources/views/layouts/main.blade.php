@extends('layouts.base')
@section('content')

    @include('notifications.alerts')

    <div class="container">
        <h1>Welcome! Your number {{auth_user()->library_card}}</h1>
        <form action="{{route('sessions.delete')}}" method="post">
            @csrf
            @method('delete')
            <button type="submit" class="btn btn-sm btn-outline-danger">LOGOUT</button>
        </form>

        <p>Your library’s website isn’t just a website, it’s an entirely separate branch of your library system.
            As such, it’s important to give your patrons an online experience with the same value as they would
            find walking through the doors of their local library. It should be clean, vibrant, and accessible to all.
            It should be a place where information and resources are easy to locate,
            with the newest and most popular things offered right up front.
            It should be a place that your patrons feel excited and happy to use.
            It’s more than a website, it’s your digital library branch.</p>

    </div>

@endsection
