<?php

use App\Http\Controllers\ApplicationsController;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\MainPageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\CategoriesController::class, 'index'])->name('home');

Route::resource('categories', \App\Http\Controllers\CategoriesController::class)->only(['index', 'show']);

Route::get('main', [MainPageController::class, 'index'])->name('main');

Route::resource('books', BooksController::class)->only(['show']);

Route::resource('books.applications', ApplicationsController::class)->only(['create', 'store']);

Route::get('/users/register', [App\Http\Controllers\UsersController::class,
    'register'])->name('users.register');
Route::post('users', [App\Http\Controllers\UsersController::class, 'store'])
    ->name('users.store');
Route::get('/login', [App\Http\Controllers\SessionsController::class, 'create'])
    ->name('sessions.login');
Route::post('/login', [App\Http\Controllers\SessionsController::class, 'store'])
    ->name('sessions.store');
Route::delete('/logout', [App\Http\Controllers\SessionsController::class, 'destroy'])
    ->name('sessions.delete');
