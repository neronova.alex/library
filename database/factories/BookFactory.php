<?php

namespace Database\Factories;

use App\Models\Book;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;


class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(2),
            'author' => $this->faker->name,
            'category_id' => rand(1, 5),
            'picture' => $this->getImage(rand(1, 9)),
            'status' => $this->faker->randomElement(['get', 'expected']),
            'return_date' => $this->faker->date()
        ];
    }

    private function getImage($image_number = 1): string
    {
        $path = storage_path() . "/pictures/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = \Intervention\Image\Facades\Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return 'pictures/' . $image_name;
    }
}
