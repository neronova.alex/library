<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Category::factory()
            ->count(5)
            ->has(\App\Models\Book::factory()
                ->count(20))->create();

        $this->call(UserTableSeeder::class);
        $this->call(ApplicationTableSeeder::class);
    }
}
